import argparse

# master comment 02

# Input: mathematical expression as a string
# Outpu: the result of the mathematical expression
def calc(expression):
    operator = expression[1]
    if len(expression.split('+')) == 2:
        operands = list(map(lambda c: int(c), expression.split('+')))
        operator = '+'
    else:
        operands = list(map(lambda c: int(c), expression.split('-')))
        operator = '-'

    res = operands[0] + operands[1] if operator == '+' else operands[0] - operands[1]
    return res
