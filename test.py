import eval

assert(eval.calc('2+2')==4)
assert(eval.calc('2-2')==0)
assert(eval.calc('2+20')==22)
